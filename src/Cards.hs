{-# LANGUAGE TemplateHaskell, GADTs, FlexibleInstances#-}
module Cards where

import Control.Lens
import Graphics.Gloss
import System.Directory(getDirectoryContents)
import Linear.V2
import qualified Data.Sequence as Sq
import qualified Data.Map.Strict as M
import Debug.Trace
import Data.Map.Strict((!))
import Data.List.Split(splitOn)
import Data.List(intersperse)
import Control.Monad(sequence,forM_)
import Graphics.Gloss.Juicy

import Assets

data Faction  = Neutral | Olunrei |  Dragon | Natanaelite | Latmarch | Roshanyrr | Druide | Vagabonds | Rysworan | Alfe | Demon | Dwarrow | Gypt | Bulu deriving (Show,Eq,Ord,Read)

data Rank     = Commun | Brave | Héroique | Légendaire | Divin | Cosmique deriving (Show,Eq,Ord,Read)

data CardType = Artifact | Event | Faction | City | Magic | Actor | Aspiration deriving (Show,Eq,Ord)

data CardDetails = CardDetails {
  _rank     :: Rank,
  _factions :: [Faction],
  _name     :: String,
  _cName    :: String,
  _desc     :: String,
  _tags     :: [Tag]
}  deriving (Show,Eq)


type Pre = String

data Final


data Card a where
  Pre     :: Pre -> FilePath -> Card Pre
  Final   :: CardType -> Picture -> CardDetails -> Card Final

data Tag  = Fire    --draconic elements 
          | Water 
          | Earth 
          | Thunder 
          | Air 
          | Aether   --alven elements
          | Light
          | Darkness
          | Endroit  -- Colce Structural elements
          | Envers
          | Creature
          | Draconic -- race
          | Raei
          | Alfeas
          | Ryswori
          | Dwarrei
          | Arathelem
          | Demonic
          deriving (Show,Eq,Read)

makeLenses ''CardDetails

instance Show (Card a) where
  show (Pre typ path)   = "precard : " ++ typ ++ " : "  ++ path
  show (Final typ _ cd) = show typ ++ " card : " ++ (cd ^.cName) ++ " (" ++ cd^.name ++ ") "

instance Eq (Card a) where
  (==) (Pre _ x) (Pre _ x') = x == x'
  (==) (Final _ _ cd) (Final _ _ cd')  = cd^.name == cd'^.name

getPath :: Card Pre -> FilePath
getPath (Pre typ path) = "cards/"++typ++"/"++path

getType :: Card Pre -> String
getType (Pre typ _) = typ

isType str card = str == getType card

zDetails = CardDetails {
  _rank     = Commun,
  _factions = [],
  _name     = "",
  _cName    = "",
  _desc     = "",
  _tags     = []
}

getPreCards :: IO [Card Pre]
getPreCards = do
  actors      <- getCardsFromFolder "actors"
  aspirations <- getCardsFromFolder "aspirations"
  cities      <- getCardsFromFolder "cities"
  events      <- getCardsFromFolder "events"
  magic       <- getCardsFromFolder "magic"
  artifacts   <- getCardsFromFolder "artifacts"
  let total = concat [actors,aspirations,cities,events,magic,artifacts]
  putStrLn $ show (length total) ++ " in total."
  return total

isCrd path = (reverse $ take 4 $ reverse path) == ".crd"


getCardsFromFolder :: FilePath -> IO [Card Pre]
getCardsFromFolder folder = do
  let cardfolder = "cards/"++ folder ++"/"
  result <- getDirectoryContents cardfolder
  let cards = map (Pre folder) $ filter (isCrd) $ filter (/="..") $ filter (/=".") $ result
  putStrLn (folder ++ " : " ++ show (length cards) )
  return cards

loadCards :: [Card Pre] ->IO (Sq.Seq (Card Final))
loadCards crds = do
  let actors      = traceShowId $ filter (isType "actors") crds 
  let aspirations = traceShowId $ filter (isType "aspirations") crds 
  let cities      = traceShowId $ filter (isType "cities") crds 
  let events      = traceShowId $ filter (isType "events") crds 
  let magic       = traceShowId $ filter (isType "magic") crds
  let artifacts   = traceShowId $ filter (isType "artifacts") crds 
  let all' =      [trace "loading actors \n"       $ sequence $ map (loadCard Actor) actors,
                  trace "loading aspirations \n"  $ sequence $ map (loadCard Aspiration) aspirations,
                  trace "loading cities \n"       $ sequence $ map (loadCard City) cities,
                  trace "loading events \n"       $ sequence $ map (loadCard Event) events,
                  trace "loading magic \n"        $ sequence $ map (loadCard Magic) magic,
                  trace "loading artifacts \n"    $ sequence $ map (loadCard Artifact) artifacts]
  unall <- sequence all'
  let final = Sq.fromList $ filterUnjust $ concat unall
  return final

parseRawCard :: String -> Maybe (M.Map String [String])
parseRawCard rawcard = result
  where
    lines = filter (/="") $ splitOn "\n" rawcard
    result
      | not $ wellFormed $ M.fromList pairs = trace "malformed card" Nothing -- check for malformed cards
      | otherwise                           = trace "ok card" $ Just $ M.fromList pairs
    wellFormed p =  not $ "malformed" `elem` (M.keys p) 
    pairs = buildCard lines
    buildCard []        = []
    buildCard (x:xs)
      | cardSelector x  = (x,info):buildCard rest
      | otherwise       = [("malformed",[])]
      where 
        (info,rest) = getInfo
        cardSelector str = head str == '-' && last str == ':'
        getInfo     = accumInfo ([],[]) xs
        accumInfo :: ([String],[String]) -> [String] -> ([String],[String])
        accumInfo (currentInfo,rest) []   = (currentInfo, rest)
        accumInfo (currentInfo,_) (x':xs')
          | cardSelector x' = (currentInfo, x':xs')
          | otherwise      = accumInfo (currentInfo++[x'],[]) xs'

loadCard :: CardType -> Card Pre ->IO (Maybe (Card Final))
loadCard typ crd = do
  let path = getPath crd
  rawcard <- (trace $ "--loading card : " ++ path)  $ readFile path
  let parsed  = trace "parsing card.." parseRawCard rawcard
  dets <- finishLoad parsed
  result <- case dets of    Nothing -> return Nothing
                            Just x  -> do
                              let nam = x^.name 
                              pic <- trace "getting picture..." $ getCardPicture nam
                              return $ Just $ Final typ pic x 
  return result

getCardPicture :: FilePath -> IO Picture
getCardPicture path = do  
  mpic <- loadJuicyPNG $! "images/drawings/" ++ path
  let res = case mpic of Nothing  -> trace "no picture found for card.." defaultDrawing
                         Just x   -> trace "picture found ! " x
  
  seq res $ putStrLn "done."
  return res



finishLoad :: Maybe (M.Map String [String]) -> IO (Maybe (CardDetails))
finishLoad parsed = case parsed of 
  Nothing   -> return Nothing
  Just amap -> do
    -- forM_ (M.toList amap ) $ \(a,b) -> do
    --   putStrLn $ show a ++ " card loaded : "++ show b
    return $ 
      Just $ zDetails & rank      .~ (read.head $ (amap ! "-rank:") :: Rank)
                      & factions  .~ (tFactions $ amap  ! "-factions:")
                      & name      .~ (head $ amap ! "-name:")
                      & cName     .~ (head $ amap ! "-cName:")
                      & desc      .~ (concat.(intersperse "\n") $ amap ! "-desc:")
                      & tags      .~ (tTags $ amap ! "-tags:")


tFactions :: [String] -> [Faction]
tFactions strs = map (\x -> ((read x):: Faction)) strs

tTags :: [String] -> [Tag]
tTags strs     = map (\x -> ((read x):: Tag)) strs

filterUnjust :: Eq a => [Maybe a] -> [a]
filterUnjust alist = let flist = filter (/=Nothing) alist in
     [ a | Just a <- flist]
    

getFaction :: Int -> Faction
getFaction 1 = Natanaelite
getFaction 2 = Roshanyrr
getFaction 3 = Olunrei
getFaction 4 = Dwarrow
getFaction 5 = Rysworan
getFaction 6 = Alfe
getFaction 7 = Demon

getPicture :: Faction -> Picture
getPicture Natanaelite = natanPic
getPicture Roshanyrr   = roshaPic 
getPicture Olunrei     = olunrPic
getPicture Dwarrow     = dwarrPic
getPicture Rysworan    = ryswoPic
getPicture Alfe        = alfenPic
getPicture Demon       = demonPic
     
     