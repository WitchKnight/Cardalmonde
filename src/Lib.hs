module Lib where

import Control.Lens
import Graphics.Gloss
import Graphics.Gloss.Interface.IO.Game
import Linear.V2
import qualified Data.Sequence as Sq
import qualified Data.Map.Strict as M
import System.Exit(exitSuccess)

import Cards
import Config
import Game
import Render
import Input

runGame = do
  (width,height,version) <- readConfig
  let conf = zConf & size .~ V2 width height 
  let background = black
      window = InWindow ("JdC v" ++ version)  (width,height) (50,50)
  playIO window background 60 zGame render handleInput update

loadGame :: Game -> IO Game
loadGame g = case g^.scene of 
  (Loading _ _ False)   -> do
    putStrLn "looking for cards..."
    precards <- getPreCards
    return $ g & scene .~ Loading precards (Sq.fromList []) True 
  (Loading prc _ True) -> do
    putStrLn "loading cards..."
    finalCards <- loadCards prc
    return $ g & scene .~ (BuildingPhase $ zBGame & bcards .~ finalCards)
  _                    -> return g

update :: Float -> Game -> IO Game
update dt g = do
  ng <- updateTime dt g
  sg <- updateScene ng
  return sg

updateScene :: Game -> IO Game
updateScene g = case g^.scene of
  Loading _ _ _ -> loadGame g
  BuildingPhase bg -> case bg^.bscene of
    Name _ _ _    -> return $ g & scene %~ shouldIPickCards 
    _             -> return g
  _             -> return g

updateTime :: Float -> Game -> IO Game
updateTime dt g = return ng
  where
    ng = g & time %~ (+ dt)
