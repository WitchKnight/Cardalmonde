{-# LANGUAGE TemplateHaskell, GADTs, FlexibleInstances#-}
module Game where

import Control.Lens
import Data.Map.Strict
import Graphics.Gloss
import qualified Data.CircularList as DC
import qualified Data.Dequeue as DQ
import Linear.V2
import System.Exit
import Debug.Trace

import Assets
import Cards
import Config

import qualified Data.Sequence as Sq

data GameScene  = BuildingPhase BGame | PlayingPhase PGame | Loading PreCards FinalCards Bool deriving (Show,Eq)

data BScene = Start | Name Int PlayerBuilder [Player] | PickCards Int [Card Final] deriving (Show,Eq)

type PreCards   = [Card Pre]

type FinalCards   = (Sq.Seq (Card Final))

data PlayerBuilder = Nam String | Fac Int String deriving (Show,Eq)

data Game = Game {
  _scene :: GameScene,
  _conf  :: Conf,
  _time  :: Float
}  deriving (Show,Eq)

data BGame = BGame {
  _bplayers :: [Player],
  _bscene   :: BScene,
  _bdecks   :: Map Player Deck,
  _bcards   :: FinalCards
}deriving (Show,Eq)

data Turn = Turn {
  _number :: Int,
  _player :: Maybe Player
}  deriving (Show,Eq)

data Player = Player {
 _pname     :: String,
 _pFact     :: Faction,
 _pid        :: Int
} deriving (Show,Eq,Ord)

data PGame = PGame {
  _board   :: Board,
  _turn    :: Turn,
  _players :: [Int]
} deriving (Show,Eq)

data Cell = Cell {
  _cards  :: [Card Final]
} deriving (Show,Eq)

data Board = Board {
  _field  :: Map Coord Cell,
  _pdecks  :: Map Player Deck,
  _vortex :: Vortex
} deriving (Show,Eq)

type Coord = V2 Int

newtype Vortex  = Vortex { vx :: [Card Final]} deriving (Show,Eq)

newtype Deck    = Deck { dx :: [Card Final]} deriving (Show,Eq)

makeLenses ''Player
makeLenses ''Turn  
makeLenses ''Cell
makeLenses ''Board
makeLenses ''PGame
makeLenses ''BGame
makeLenses ''Game

zGame = Game {
  _scene = Loading [] (Sq.fromList []) False,
  _conf  = zConf,
  _time  = 0
}

zPlayer = Player {
  _pname = "",
  _pFact = Neutral,
  _pid    = 0
}


zBGame = BGame {
  _bplayers = [],
  _bscene   = Start,
  _bdecks   = fromList [],
  _bcards   = Sq.empty
}

zPGame = PGame {
  _board    = zBoard,
  _turn     = zTurn,
  _players  = []
}

zBoard = Board {
  _field    = fromList [],
  _pdecks   = fromList [],
  _vortex   = Vortex []
}

zTurn = Turn {
  _number = 0,
  _player = Nothing
}
-- utility functions


-- general game logic
exitGame :: a -> IO a
exitGame a = do
  putStrLn "quitting game.."
  exitSuccess
  return a

esc :: Game -> IO Game
esc g = case g^.scene of
  Loading _ _ False   -> return g
  Loading _ _ True    -> exitGame g
  _         -> exitGame g

cancel :: Game -> IO Game
cancel g = case g^.scene of
  BuildingPhase bg -> return $ g & scene %~ cancelBG
  _ -> return g

confirm :: Game -> IO Game
confirm g = case g^.scene of
  BuildingPhase bg -> return $ g & scene %~ confirmBG
  _ -> return g

goUp :: Game -> IO Game
goUp g = return g
goDown g = return g
-- building phase game logic

start :: BGame -> Bool
start bg = bg^.bscene == Start

numPlayers :: Int -> GameScene -> GameScene
numPlayers n (BuildingPhase bg) = BuildingPhase $ bg & bscene .~ Name n (Nam "") []
numPlayers _ other = other

chooseFact :: Int -> GameScene -> GameScene
chooseFact n (BuildingPhase bg) = BuildingPhase $ bg & bscene %~ chooseFact' n
chooseFact' fn (Name n (Fac _ nam) pl) = Name n (Fac fn nam) pl

addChar :: Char -> GameScene -> GameScene
addChar c (BuildingPhase bg) = BuildingPhase $ bg & bscene %~ addChar' c
addChar' c (Name n (Nam name) pl) = (Name n (Nam $ name++[c]) pl)

confirmBG :: GameScene -> GameScene
confirmBG  (BuildingPhase bg) = case bg^.bscene of 
  Start -> (BuildingPhase $ bg & bscene .~ Name 2 (Nam "") [])
  Name n (Nam nam )   x   -> BuildingPhase $ bg & bscene .~ Name n (Fac 1 nam) x
  Name n (Fac 0 nam)  x   -> BuildingPhase $ bg & bscene .~ Name n (Fac 1 nam) x
  Name n (Fac fn nam) pl  -> BuildingPhase $ bg & bscene .~ Name n (Nam "") (x:pl)
    where
      x = zPlayer & pname .~ nam
                  & pFact .~ getFaction fn
                  & pid   .~ length pl

cancelBG :: GameScene -> GameScene
cancelBG  (BuildingPhase bg) = case bg^.bscene of 
  Start -> BuildingPhase $ bg & bscene .~ Start
  Name n (Nam nam ) _     -> BuildingPhase $ bg & bscene .~ Start
  Name n (Fac _ nam) x  -> BuildingPhase $ bg & bscene .~ Name n (Nam nam) x

shouldIPickCards ::  GameScene -> GameScene 
shouldIPickCards (BuildingPhase bg)
  | length pl <  n = (BuildingPhase bg)
  | otherwise = BuildingPhase $  bg & bscene   .~ (PickCards 0 [])
                                    & bplayers .~ pl
  where
    (Name n _ pl) = bg^.bscene