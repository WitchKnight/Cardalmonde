{-# LANGUAGE TemplateHaskell #-}
module Config where

import Control.Lens
import Linear.V2
import Data.Map.Strict  hiding (size)
import Data.List.Split

readConfig :: IO (Int,Int, String)
readConfig = do
  conf <- readFile ("config/conf.ini")
  let attributes = attrs conf
  let width   = attributes ! "width"
      height  = attributes ! "height"
      version = attributes ! "version"

  return (read width :: Int, read height :: Int, version)

attrs :: String -> Map String String
attrs conf = fromList attrpairs
  where 
    prel = splitOn "\n" conf
    preattrpairs = [ splitOn "=" x | x <- prel]
    attrpairs = [(x, y) | [x,y] <- preattrpairs]


data Conf = Conf {
    _size :: V2 Int
} deriving (Show,Eq)

makeLenses ''Conf

zConf = Conf {
  _size = (V2 1024 768)
}

resize :: V2 Int -> Conf -> Conf
resize ns cf = cf & size .~ ns