module Input where

import Graphics.Gloss.Interface.IO.Game
import Debug.Trace
import Linear.V2
import Control.Lens

import Game
import Config

handleInput :: Event -> Game -> IO Game
handleInput (EventResize (x,y)) g = return $ g & conf %~ resize newsize
  where
      newsize = V2 (fromIntegral x) (fromIntegral y)

        
handleInput (EventKey (SpecialKey KeyEsc ) Down mod _) g
  | shift mod == Down = esc g
  | otherwise = cancel g        

handleInput (EventKey (SpecialKey KeyEnter) Down _ _) g = confirm g
handleInput (EventKey (SpecialKey KeyBackspace) Down x y) g = handleInput (EventKey (SpecialKey KeyLeft) Down x y) g

handleInput (EventKey (SpecialKey KeyLeft) Down _ _) g = case g^.scene of
  BuildingPhase bg    -> return (g & scene .~ handleCharBuilding bg)
    where
      handleCharBuilding bg = case bg^.bscene of
        Name n (Nam "") pl    -> BuildingPhase bg
        Name n (Nam nam) pl   -> BuildingPhase (bg & bscene %~ removeLetter)
        _                     -> BuildingPhase bg
      removeLetter (Name n (Nam nam) pl) = Name n (Nam (reverse.tail.reverse $ nam)) pl  
  _                       -> return g
handleInput (EventKey (Char x) Down _ _) g = case g^.scene of
  Loading _ _ _       -> return g
  BuildingPhase bg    -> handleCharBuilding
    where  
      hStartB
        | x `elem` ['1','2','3','4','5'] = return $ g & scene %~ numPlayers (read [x] :: Int)
        | otherwise = return g
      hNameB  = return $ g & scene %~ addChar x
      hFactB
        | x `elem` ['1','2','3','4','5','6','7'] = return $ g & scene %~ chooseFact (read [x] :: Int)
        | otherwise = return g
      handleCharBuilding = case bg^.bscene of
        Start           ->  hStartB
        Name n (Nam nam) pl   -> hNameB
        Name n (Fac _ nam) pl -> hFactB  
        
handleInput (EventKey (SpecialKey KeyDown) Down _ _) g = goDown g
handleInput (EventKey (SpecialKey KeyUp) Down _ _) g = goUp g



handleInput _ g = return g