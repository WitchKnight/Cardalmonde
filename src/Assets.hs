module Assets where

import Graphics.Gloss
import Graphics.Gloss.Game

-- game ui

symfolder = "images/symboles/"

loadingPic1 = png $ symfolder ++ "loading.png"
loadingPic2 = png $ symfolder ++ "loading2.png" 
almondePic = png $ symfolder ++ "Almonde.png"


-- cards

tempfolder = "images/templates/"
drawfolder = "images/drawings/"

defaultDrawing = png $ drawfolder ++ "defaultDrawing.png"
commonTemp = png $ tempfolder ++ "Common.png"


natanPic = png $ symfolder ++ "natanaelite.png"
roshaPic = png $ symfolder ++ "roshanyrr.png"
olunrPic = png $ symfolder ++ "olunrei.png"
dwarrPic = png $ symfolder ++ "dwarrow.png"
ryswoPic = png $ symfolder ++ "rysworan.png"
alfenPic = png $ symfolder ++ "alfe.png"
demonPic = png $ symfolder ++ "demon.png"