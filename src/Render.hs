module Render where

import Control.Lens
import Linear.V2
import Graphics.Gloss
import Game
import Cards
import Config
import Assets

fI = fromIntegral


bounce :: Float -> Float -> Float
bounce x ul | times `rem` 2 == 1 = (ul*(1+ fI times)- x)
            | otherwise = x - (ul*(fI times))
  where
    div' :: Float
    div' = x/ul
    times = truncate div'


rotateBounce b r = rotate (bounce b r)


place :: Float -> Float -> Float -> Float -> Picture -> Picture
place w h x y  = translate (w*x-w/2) (h*y-h/2)

placeBounce :: Float -> Float -> Float -> Float -> Float -> Picture -> Picture
placeBounce w h b x y  = place w h (bounce b x) (bounce b y)

diagBounce w h b x = placeBounce w h b x x 


scaleBounceBetween :: Float -> Float -> Float -> Float -> Float -> Picture -> Picture
scaleBounceBetween b x1 x2 y1 y2 = scale (x1 + bounce b (x2-x1)) (y1 + bounce b (y2-y1))

scaleBounceBetween' b x1 x2 = scaleBounceBetween b x1 x2 x1 x2 
 

scaleBounce :: Float -> Float -> Float -> Picture -> Picture
scaleBounce b x y = scale (bounce b x) (bounce b y)

scaleBounce' b x = scaleBounce b x x 
  
render :: Game -> IO Picture
render g =  let (V2 w h)    = g^.conf^.size 
                (w',h')     = (fI w, fI h)
                place'      = place w' h'
                pBounce     = placeBounce w' h'
                t           = g^.time
                timePBounce = pBounce t
                timeBounce  = bounce t
                timeSB      = scaleBounce t
                timeSB'     = scaleBounce' t
                timeSBB     = scaleBounceBetween t
                timeSBB'    = scaleBounceBetween' t
                
                loadingPics = pictures [rotate (t*120) $ timeSBB' 1 1.4 $ loadingPic1, rotate (-t*120) $ timeSBB' 2 2.4 $ loadingPic2]
                loading     = place' 0.1 0.1 $ pictures [ loadingPics, almondePic, translate 100 (-10) $ scale 0.25 0.2 $ color white $ text "Loading.."] 
                
                buildingIndic = pictures [rotateBounce (t*120) 180 $ color (makeColor 1 1 1 0.5)$  timeSBB' 1 1.3 $ loadingPic1, rotate (-t*120) $ timeSBB' 1.2 2.2 $ loadingPic2]
                factionIndic n =  place' 0.1 (0.8-(fromIntegral n)*0.1) $ pictures [buildingIndic, getPicture $ getFaction n]
                buildRender bg = case bg^.bscene of
                  Start             -> introText1
                  Name n (Nam name) pl        -> pictures [ place' 0.2 0.6 $ buildingIndic, introText2 (length pl), putOtext name]
                  Name n (Fac n' nam) _     -> pictures [ factionIndic n', introText3  nam, allFacts]
                  _                 -> blank
                introText1      = place' 0.1 0.8 $ ( color white $ scale 0.1 0.1 $ text "Combien de joueurs ? (Entrer un nombre de 1 à 5)")
                introText2 n    = place' 0.1 0.8 $ ( color white $ scale 0.1 0.1 $ text $ "Entrer le nom du joueur " ++ (show $ 1 + n))
                introText3 nm   = place' 0.1 0.8 $ ( color white $ scale 0.1 0.1 $ text $ "Choisis ta faction, "++nm)
                putOtext   n    = place' 0.2 0.6 $ ( color white $ scale 0.12 0.12 $ text n)
                allFacts        = pictures [ place' 0.2 (0.8-n*0.1) $ color white $ scale 0.12 0.12 $ text $ (show (truncate n) ++ "- " ++ show fac) | (fac,n) <- zip [Natanaelite,Roshanyrr,Olunrei,Dwarrow,Rysworan,Alfe,Demon] [1..7] ]

            in case g^.scene of
    Loading _ _ _           -> return $! loading
    BuildingPhase bg        -> return $! buildRender bg
      


    _                       -> return blank